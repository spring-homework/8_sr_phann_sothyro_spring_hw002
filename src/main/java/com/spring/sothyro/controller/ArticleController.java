package com.spring.sothyro.controller;

import com.spring.sothyro.model.Article;
import com.spring.sothyro.service.ArticleService;
import com.spring.sothyro.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class ArticleController {
    @Autowired
    FileStorageService fileStorageService;

    @Autowired
    ArticleService articleService;

    @GetMapping
    public String index(Model model){
        model.addAttribute("article", new Article());
        model.addAttribute("articles", articleService.getAllArticle());
        return "index";
    }

    @GetMapping("/form-add")
    public String showFormAdd(Model model){
        model.addAttribute("article", new Article());
        return "add";
    }

    @PostMapping("/handle-add")
    public String handleAdd(@ModelAttribute @Valid Article article, BindingResult bindingResult){
        if(article.getFile().isEmpty()){
            article.setUrlImg("http://localhost:8080/images/defaulImage.png");
        }else {
            try {
                String filename = "http://localhost:8080/images/"+ fileStorageService.saveFile(article.getFile());
                System.out.println("filename: " + filename);
                article.setUrlImg(filename);
            }catch (Exception e){
                System.out.println("Error with the images upload" + e.getMessage());
            }
        }

        if(bindingResult.hasErrors()){
            return "add";
        }
        articleService.addArticleMethod(article);

        return "redirect:/";
    }

    @GetMapping("/view-article/{id}")
    public String viewArticle(@PathVariable int id,Model model){
        Article resultArticle = articleService.findArticleById(id);

        System.out.println("result article : " + resultArticle);
        model.addAttribute("article", resultArticle);

        return "view";
    }

    @GetMapping("/update-article/{id}")
    public String updateArticle(@PathVariable int id, Model model){
        Article resultArticle = articleService.findArticleById(id);
        System.out.println("result article : " + resultArticle);
        model.addAttribute("article", resultArticle);
        model.addAttribute("id", id);
        return "update";
    }

    @GetMapping("/delete-article/{id}")
    public String deleteArticle(@PathVariable int id, Model model){
        articleService.deleteArticleMethod(id);
        return "redirect:/";
    }

    @PostMapping("/handle-update/{id}")
    public String handleUpdate(@PathVariable int id, @ModelAttribute @Valid Article article, BindingResult bindingResult){
        Article resultArticle = articleService.findArticleById(id);
        if(article.getFile().isEmpty()){
            article.setUrlImg(resultArticle.getUrlImg());
        }else {
            try {
                String filename = "http://localhost:8080/images/"+ fileStorageService.saveFile(article.getFile());
                System.out.println("filename: " + filename);
                System.out.println(article);
                article.setUrlImg(filename);
            }catch (Exception e){
                System.out.println("Error with the images upload" + e.getMessage());
            }
        }

        if(bindingResult.hasErrors()){
            return "update";
        }
        articleService.updateArticleMethod(id, article);

        return "redirect:/";
    }
}
