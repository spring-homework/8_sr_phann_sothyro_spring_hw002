package com.spring.sothyro.service;

import com.spring.sothyro.model.Article;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ArticleService {

    public List<Article> getAllArticle();
    public Article findArticleById(int id);
    public void addArticleMethod(Article article);
    public void updateArticleMethod(int id, Article article);
    public void deleteArticleMethod(int id);
}